//const dotenv = require('dotenv');
//dotenv.config();

const Discord = require("discord.js")
const util = require("./util/utils")
const anilist = require("./helpers/anilist")
const games = require("./helpers/games")
const tenorGifs = require("./helpers/tenor")
const others = require('./helpers/others')
const ttext = require("./helpers/translate")
const player = require("./helpers/player")
const poke = require("./helpers/pokedex")

const help = require("./util/help.json")

const client = new Discord.Client()

const prefix = "chun!"

client.on('ready', () => {
    console.log("Estoy listo")
})

client.on('guildMemberAdd', member => {
    const channel = member.guild.channels.cache.find(ch => ch.name === 'general');
    if (!channel) return;
    channel.send(`Bienvenid@ al servidor, ${member} 😉`);
})

client.on("message", function(message){
    if(message.author.bot) return;
    if(!message.content.startsWith(prefix)) return;

    const commandBody = message.content.slice(prefix.length)
    const args = commandBody.trim().split(/ +/g)
    const command = args.shift().toLowerCase()
    const argsMerged = args.join(' ')

    const user = message.mentions.users.first()
    const member = message.guild.member(user)

    const categories = ["Action", "Adventure", "Comedy", "Drama", "Ecchi", "Fantasy", "Horror", "Mahou Shoujo", "Mecha", "Music", 
    "Mystery", "Psychological", "Romance", "Sci-Fi", "Slice of Life", "Sports", "Supernatural", "Thriller","action", "adventure", "comedy", "drama", "ecchi", "fantasy", "horror", "mahou shoujo", "mecha", "music", 
    "mystery", "psychological", "romance", "sci-fi", "slice of life", "sports", "supernatural", "thriller"]

    if(command === "kick"){
        if(user){
            if(member){
                member.kick('Se ha expulsado por mal comportamiento')
                .then(() => {
                    message.channel.send(`Se ha expulsado a ${user.tag}`)
                })
                .catch(err => {
                    message.reply("No se pudo expulsar al miembro")
                    console.log(err)
                })
            }

            else {
                message.reply("El usuario no existe en la sala!")
            }
        }

        else {
            message.reply("No mencionaste un usuario para expulsar!")
        }
    }

    else if(command === "randommanga"){
        if(!args.length){
            message.reply("No incluiste una categoría que buscar")
        }
        else {
            
            if(categories.includes(argsMerged)){
                anilist.getRandomManga(argsMerged)
                .then(async res => {
                    const translatedText = await ttext.watsonTranslate(res.description)

                    await message.channel.send({embed: {
                        color: 'C60000',
                        title: `Nombre: ${res.title.romaji}`,
                        fields: [
                            {
                                name: "Inicio",
                                value: `${res.startDate.day}-${res.startDate.month}-${res.startDate.year}`,
                                "inline": true
                            },
                            {
                                name: "Final",
                                value: `${res.endDate.day}-${res.endDate.month}-${res.endDate.year}`,
                                "inline": true
                            },
                            {
                                name: "Capitulos",
                                value: `${res.chapters}`,
                                "inline": true
                            },
                            {
                                name: "Volumenes",
                                value: `${res.volumes}`,
                                "inline": true
                            }
                        ],
                        image: {
                            url: res.coverImage.large
                        }}
                    })

                    await message.channel.send(`**Descripción:** ${translatedText}`,{split: {char: ' '}})
                })
            }
            else {
                message.reply('categoría no encontrada, las categorías son: Action, Adventure, Comedy, Drama, Ecchi, Fantasy, Horror, Mahou Shoujo, Mecha, Music, Mystery, Psychological, Romance, Sci-Fi, Slice of Life, Sports, Supernatural, Thriller')
            }
        }
    }

    else if (command === "randomanime"){
        if(!args.length){
            message.reply("No incluiste una categoría que buscar")
        }
        else {
            
            if(categories.includes(argsMerged)){
                anilist.getRandomAnime(argsMerged)
                .then(async res => {
                    const translatedText = await ttext.watsonTranslate(res.description)

                    await message.channel.send({embed: {
                        color: 'C60000',
                        title: `Nombre: ${res.title.romaji}`,
                        fields: [
                            {
                                name: "Inicio",
                                value: `${res.startDate.day}-${res.startDate.month}-${res.startDate.year}`,
                                "inline": true
                            },
                            {
                                name: "Final",
                                value: `${res.endDate.day}-${res.endDate.month}-${res.endDate.year}`,
                                "inline": true
                            },
                            {
                                name: "Episodios",
                                value: `${res.episodes}`,
                                "inline": true
                            },
                            {
                                name: "Duración",
                                value: `${res.duration} minutos`,
                                "inline": true
                            }
                        ],
                        image: {
                            url: res.coverImage.large
                        }}
                    })

                    await message.channel.send(`**Descripción:** ${translatedText}`,{split: {char: ' '}})
                })
            }
            else {
                message.reply('categoría no encontrada, las categorías son: Action, Adventure, Comedy, Drama, Ecchi, Fantasy, Horror, Mahou Shoujo, Mecha, Music, Mystery, Psychological, Romance, Sci-Fi, Slice of Life, Sports, Supernatural, Thriller')
            }
        }
    }

    else if (command === "manga"){
        if(!args.length){
            message.reply("No incluiste un título para buscar")
        }
        else {
            anilist.getManga(argsMerged)
                .then(async res => {
                    const translatedText = await ttext.watsonTranslate(res.description)
                    
                    await message.channel.send({embed: {
                        color: 'C60000',
                        title: `Nombre: ${res.title.romaji}`,
                        fields: [
                            {
                                name: "Inicio",
                                value: `${res.startDate.day}-${res.startDate.month}-${res.startDate.year}`,
                                "inline": true
                            },
                            {
                                name: "Final",
                                value: `${res.endDate.day}-${res.endDate.month}-${res.endDate.year}`,
                                "inline": true
                            },
                            {
                                name: "Capitulos",
                                value: `${res.chapters}`,
                                "inline": true
                            },
                            {
                                name: "Volumenes",
                                value: `${res.volumes}`,
                                "inline": true
                            }
                        ],
                        image: {
                            url: res.coverImage.large
                        }},
                    })

                    await message.channel.send(`**Descripción:** ${translatedText}`,{split: {char: ' '}})
                })
                .catch(async err => {
                    await message.reply("no se ha encontrado el título")
                })
        }
    }

    else if (command === "anime"){
        if(!args.length){
            message.reply("No incluiste un título para buscar")
        }
        else {
            anilist.getAnime(argsMerged)
                .then(async res => {
                    const translatedText = await ttext.watsonTranslate(res.description)
                    
                    await message.channel.send({embed: {
                        color: 'C60000',
                        title: `Nombre: ${res.title.romaji}`,
                        fields: [
                            {
                                name: "Inicio",
                                value: `${res.startDate.day}-${res.startDate.month}-${res.startDate.year}`,
                                "inline": true
                            },
                            {
                                name: "Final",
                                value: `${res.endDate.day}-${res.endDate.month}-${res.endDate.year}`,
                                "inline": true
                            },
                            {
                                name: "Episodios",
                                value: `${res.episodes}`,
                                "inline": true
                            },
                            {
                                name: "Duración",
                                value: `${res.duration} minutos`,
                                "inline": true
                            }
                        ],
                        image: {
                            url: res.coverImage.large
                        }}
                    })

                    await message.channel.send(`**Descripción:** ${translatedText}`,{split: {char: ' '}})
                })
                .catch(async err => {
                    await message.reply("no se ha encontrado el título")
                })
        }
    }

    else if(command === "staff"){
        if(!args.length){
            message.reply("No incluiste un nombre para buscar")
        }
        else {
            anilist.getStaff(argsMerged)
                .then(async res => {
                    let producerWork = []
                    const translatedText = await ttext.watsonTranslate(res.description)

                    res.staffMedia.edges.forEach(el => {
                        producerWork.push(`${el.node.title.romaji} - ${el.staffRole}`)
                    })

                    await message.channel.send({embed: {
                        color: 'C60000',
                        title: `Nombre: ${res.name.full}`,
                        description: `**Roles en Producción de anime o manga:**\n\n${producerWork.join("\n\n")}`,
                        image: {
                            url: res.image.large
                        }}
                    })

                    await message.channel.send(`**Descripción:** ${translatedText}`,{split: {char: ' '}})

                })
        }
    }

    else if(command === "voiceactor"){
        if(!args.length){
            message.reply("No incluiste un nombre para buscar")
        }
        else {
            anilist.getVoiceActor(argsMerged)
                .then(async res => {
                    let actingWork = []
                    const translatedText = await ttext.watsonTranslate(res.description)

                    res.characterMedia.edges.forEach(el => {
                        actingWork.push(`${el.characters[0].name.full} - ${el.node.title.romaji}`)
                    })

                    await message.channel.send({embed: {
                        color: 'C60000',
                        title: `Nombre: ${res.name.full}`,
                        description: `**Roles en Producción de anime:**\n\n${actingWork.join("\n\n")}`,
                        image: {
                            url: res.image.large
                        }}
                    })

                    await message.channel.send(`**Descripción:** ${translatedText}`,{split: {char: ' '}})

                })
        }
    }

    else if(command === "nextepisodes"){
        let listEpisodes = ''

        anilist.getAiringEpisodes()
            .then(async res => {
                res.forEach(el =>{
                    const resDays = util.sinceToday(el.nextAiringEpisode.timeUntilAiring)
                    listEpisodes += `**${el.title.romaji} - Formato: ${el.format}**\n*El episodio N° ${el.nextAiringEpisode.episode} ${resDays}*\n\n`
                })

                await message.channel.send(`**Proximos capítulos a estrenar**\n\n${listEpisodes}`, { split: {char: ' '}})
            })
    }

    else if (command === "character"){
        if(!args.length){
            message.reply("No incluiste un personaje para buscar")
        }
        else {
            anilist.getCharacter(argsMerged)
                .then(async res => {
                    const translatedText = await ttext.watsonTranslate(res.description)

                    if(res.name.last == null && res.name.native != null) {
                        await message.channel.send(`**Nombre:** ${res.name.first} \n**Romaji:** ${res.name.native}`, {files:[res.image.large]})
                    }
                
                    else if(res.name.last == null && res.name.native == null){
                        await message.channel.send(`**Nombre:** ${res.name.first}`, {files:[res.image.large]})
                    }
                
                    else {
                        await message.channel.send(`**Nombre:** ${res.name.first} ${res.name.last} \n**Romaji:** ${res.name.native}`, {files:[res.image.large]})
                    }
                    
                    await message.channel.send(translatedText, { split: {char: ' '}})
                })
                .catch(async err => {
                    await message.reply("no se ha encontrado al personaje")
                })
        }
    }

    else if(command === "eightball"){
        if(!args.length){
            message.reply("No hiciste ninguna pregunta")
        }

        else {
            const response = games.eightBall()
            message.reply(response)
        }
    }

    else if(command === "flipcoin"){
        const response = games.flipCoin()
        message.reply(response)
    }

    else if(command === "rsp"){
        if(!args.length){
            message.reply("No elegiste minguna opción")
        }

        else {
            const response = games.rsp(argsMerged)
            message.reply(response)
        }
    }

    else if(command === "horoscopo"){
        if(!args.length){
            message.reply("No incluiste tu signo")
        }

        else {
            others.getHoroscope(argsMerged)
                .then(async res => {
                    const translatedText = await ttext.watsonTranslate(res.description)

                    await message.reply(`${translatedText}\n\nTu número de la suerte es: ${res.lucky_number}`, { split: {char: ' '}})
                })
        }
    }

    else if(command == "loli"){
        tenorGifs.loli()
            .then(async res => {
                await message.reply("Eso no le gusta al FBI oni-chan 😔", {files:[res]})
            })
    }

    else if(command == "husbando"){
        tenorGifs.husbando()
            .then(async res => {
                await message.reply("Se que es real, se que me ama uwu", {files:[res]})
            })
    }

    else if(command == "abrazo"){
        if(user){
            if(member){
                tenorGifs.abrazo()
                .then(async res => {
                    await message.reply(`*le da un abrazo a ${user.tag} uwu*`, {files:[res]})
                })
                .catch(err => {
                    message.reply(`*no podrás abrazar a ${user.tag} por ahora unu, intenta más tarde*`)
                    console.log(err)
                })
            }

            else {
                message.reply("El usuario no existe en la sala!")
            }
        }

        else {
            message.reply("no indicaste a quien quieres enviar un abrazo")
        }
    }

    else if(command == "caricia"){
        if(user){
            if(member){
                tenorGifs.caricia()
                .then(async res => {
                    await message.reply(`*le da una caricia a ${user.tag} uwu*`, {files:[res]})
                })
                .catch(err => {
                    message.reply(`*no podrás acariciar a ${user.tag} por ahora unu, intenta más tarde*`)
                    console.log(err)
                })
            }

            else {
                message.reply("El usuario no existe en la sala!")
            }
        }

        else {
            message.reply("no indicaste a quien quieres dar una caricia")
        }
    }

    else if(command == "beso"){
        if(user){
            if(member){
                tenorGifs.beso()
                .then(async res => {
                    await message.reply(`*le da un beso a ${user.tag} uwu*`, {files:[res]})
                })
                .catch(err => {
                    message.reply(`*no podrás besar a ${user.tag} por ahora unu, intenta más tarde*`)
                    console.log(err)
                })
            }

            else {
                message.reply("El usuario no existe en la sala!")
            }
        }

        else {
            message.reply("no indicaste a quien quieres dar un beso")
        }
    }

    else if(command == "addsong"){
        const voiceChannel = message.member.voice.channel

        if(!voiceChannel){
            message.channel.send("Necesitas estar en un canal de voz para reproducir música")
        }
        else {
            const permissions = voiceChannel.permissionsFor(message.client.user)
            if(!permissions.has("CONNECT") || !permissions.has("SPEAK")){
                message.channel.send("Necesito permisos para unirme y hablar en un canal de voz!")
            }
            else {
                if(!args.length){
                    message.reply("No incluiste ninguna canción")
                }
        
                else {
                    player.execute(argsMerged,message,voiceChannel)
                }
            }
        }

    }

    else if(command == "pausesong"){
        const voiceChannel = message.member.voice.channel

        if(!voiceChannel){
            message.channel.send("Necesitas estar en un canal de voz para pausar la canción")
        }
        else {
            player.pause(message)
        }
    }

    else if(command == "resumesong"){
        const voiceChannel = message.member.voice.channel

        if(!voiceChannel){
            message.channel.send("Necesitas estar en un canal de voz para pausar la canción")
        }
        else {
            player.resume(message)
        }
    }

    else if(command == "stopsong"){
        const voiceChannel = message.member.voice.channel

        if(!voiceChannel){
            message.channel.send("Debes estar en un canal de voz para detener la música")
        }
        else {
            player.stop(message)
        }
    }

    else if(command === "pokedex"){
        if(!args.length){
            message.reply("No haz incluido un pokemon para buscar")
        }

        else {
            poke.getInfo(argsMerged,message)
        }
    }

    else if(command === "listcommands"){
        message.channel.send({embed: {
            color: 'F1C10C',
            title: "Comandos de Chunchunmaru",
            description: 'Hola, soy Chunchunmaru (✿˵◕ ɜ◕˵) y aquí tienes mi lista de comandos. Puedes ver una descripción más detallada de cada comando por separado con `chun!help nombre-del-comando`',
            fields: [
                {
                    name: "Comandos de Admin",
                    value: '`kick`'
                },
                {
                    name: "Comandos sobre Música",
                    value: '`addsong` `pausesong` `resumesong` `stopsong`'
                },
                {
                    name: "Comandos sobre Anime",
                    value: '`randommanga` `randomanime` `manga` `anime` `staff` `voiceactor` `nextepisodes` `mangacharacter` `loli` `husbando`'
                },
                {
                    name: "Comandos divertidos",
                    value: '`eightball` `flipcoin` `rsp` `horoscopo`'
                },
                {
                    name: "Comandos de interacción",
                    value: '`abrazo` `caricia` `beso`'
                },
                {
                    name: "Comandos de Búsqueda",
                    value: '`pokedex`'
                },
                {
                    name: "Otros comandos",
                    value: '`list` `help`'
                }
            ]}
        })
    }

    else if(command === "help"){
        if(!args.length){
            message.reply("No incluiste un comando para buscar")
        }
        else {
            if(help.commands.some(command => command.name === argsMerged)){
                const helpArr = help.commands.find(obj => {
                    return obj.name === argsMerged
                })
                const title = helpArr.name
                const description = helpArr.description
                const example = helpArr.example

                message.channel.send({embed: {
                    color: 'F1C10C',
                    title: `${title}`,
                    description: `${description}`,
                    fields: [
                        {
                            name: "Ejemplo de uso de este comando",
                            value: `${example}`
                        }
                    ]}
                })
            }
            else{
                message.reply("No puedo encontrar el comando")
            }
        }
    }

    else {
        message.reply(`Comando no encontrado`)
    }
})

client.login(process.env.BOT_TOKEN)