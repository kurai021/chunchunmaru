const {request, gql} = require("graphql-request")
const endpoint = process.env.ANILIST_ENDPOINT

function getRandomManga(data){
    const query = gql`{
        Page {
            media(genre: "${data}", type: MANGA, format: MANGA, isAdult: false) {
                title {
                    romaji
                    native
                }
                coverImage {
                    medium
                    large
                }
                description(asHtml: false)
                format
                type
                averageScore
                chapters
                volumes
                startDate {
                    year
                    month
                    day
                }
                endDate {
                    year
                    month
                    day
                }
            }
        }
    }`

    return request(endpoint, query)
        .then(res => {
            const keys = Object.keys(res.Page.media)
            return res.Page.media[keys[ keys.length * Math.random() << 0]];
        })

}

function getRandomAnime(data){
    const query = gql`{
        Page {
            media(genre: "${data}", type: ANIME, format: TV, isAdult: false) {
                title {
                    romaji
                    native
                }
                coverImage {
                    medium
                    large
                }
                description(asHtml: false)
                format
                type
                averageScore
                episodes
                duration
                startDate {
                    year
                    month
                    day
                }
                endDate {
                    year
                    month
                    day
                }
            }
        }
    }`


    return request(endpoint, query)
        .then(res => {
            const keys = Object.keys(res.Page.media)
            return res.Page.media[keys[ keys.length * Math.random() << 0]];
        })

}

function getStaff(data){
    const query = gql`{
        Page {
          staff(search: "${data}") {
            id
            name {
              full
            }
            image {
              large
            }
            description(asHtml: false)
            staffMedia {
              edges {
                node {
                  title {
                    romaji
                  }
                  type
                  siteUrl
                }
                staffRole
              }
            }
          }
        }
      }`

    return request(endpoint, query)
    .then(res => {
        return res.Page.staff[0];
    })
}

function getVoiceActor(data){
    const query = gql`{
        Page {
          staff(search: "${data}") {
            id
            name {
              full
            }
            image {
              large
            }
            description(asHtml: false)
            characterMedia {
              edges {
                characters {
                  id
                  name {
                    full
                  }
                }
                node {
                  title {
                    romaji
                  }
                  type
                  siteUrl
                }
              }
            }
          }
        }
      }`

    return request(endpoint, query)
    .then(res => {
        return res.Page.staff[0];
    })
}

function getAiringEpisodes(){
    const query = gql`{
        Page{
            media(type: ANIME, isAdult: false, countryOfOrigin: JP, status: RELEASING) {
                title {
                    romaji
                    native
                }
                coverImage {
                    large
                }
                format
                type
            	nextAiringEpisode {
            		id
                    episode
                    airingAt
                    timeUntilAiring
            	}
            }
        }
    }`

    return request(endpoint, query)
    .then(res => {
        let sanitizedMedia = []
        res.Page.media.forEach(el => {
            if(el.nextAiringEpisode != null){
                sanitizedMedia.push(el)
            }
        })

        return sanitizedMedia;
    })
}

function getManga(data){
    const query = gql`{
        Page {
            media(search: "${data}", type: MANGA, format: MANGA, isAdult: false) {
                title {
                    romaji
                    native
                }
                coverImage {
                    medium
                    large
                }
                description
                format
                type
                averageScore
                chapters
                volumes
                startDate {
                    year
                    month
                    day
                }
                endDate {
                    year
                    month
                    day
                }
            }
        }
    }`

    return request(endpoint, query)
      .then(res => {
          return res.Page.media[0];
      })

}

function getAnime(data){
    const query = gql`{
        Page {
            media(search: "${data}", type: ANIME, format: TV, isAdult: false) {
                title {
                    romaji
                    native
                }
                coverImage {
                    medium
                    large
                }
                description
                format
                type
                averageScore
                episodes
                duration
                startDate {
                    year
                    month
                    day
                }
                endDate {
                    year
                    month
                    day
                }
            }
        }
    }`

    return request(endpoint, query)
      .then(res => {
          return res.Page.media[0];
      })

}

function getCharacter(data){
    const query = gql`{
        Character(search: "${data}"){
          id
          name {
            first
            last
            native
          }
          image {
            large
          }
          description(asHtml: false)
        }
      }`

    return request(endpoint, query)
      .then(res => {
          return res.Character;
      })
}

module.exports = {
    getRandomManga:getRandomManga,
    getRandomAnime:getRandomAnime,
    getStaff:getStaff,
    getVoiceActor:getVoiceActor,
    getAiringEpisodes:getAiringEpisodes,
    getManga:getManga,
    getAnime:getAnime,
    getCharacter:getCharacter
}