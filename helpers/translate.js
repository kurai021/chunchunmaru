const LanguageTranslatorV3 = require('ibm-watson/language-translator/v3');
const {IamAuthenticator} = require("ibm-watson/auth")

const languageTranslator = new LanguageTranslatorV3({
    authenticator: new IamAuthenticator({ apikey: process.env.LANGUAGE_TRANSLATOR_APIKEY }),
    serviceUrl: process.env.LANGUAGE_TRANSLATOR_URL,
    version: process.env.LANGUAGE_TRANSLATOR_VERSION,
  });

function watsonTranslate(data){
    return languageTranslator.translate(
        {
          text: data,
          source: 'en',
          target: 'es'
        })
        .then(response => {
            return JSON.stringify(response.result.translations[0].translation);
        })
}

module.exports = {
    watsonTranslate:watsonTranslate
}