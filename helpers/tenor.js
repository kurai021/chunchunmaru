const tenor = require("tenorjs")

const Tenor = tenor.client({
    "Key": process.env.TENOR_API,
    "Filter": "off",
    "Locale": "en_US",
    "MediaFilter": "minimal",
    "DateFormat": "D/MM/YYYY - H:mm:ss A"
})

function loli(){
    return Tenor.Search.Random("loli anime","1")
        .then(data => {
            return data[0].media[0].gif.url
        })
        .catch(err => {
            console.log(err)
        })
}

function husbando(){
    return Tenor.Search.Random("%23anime %23boy","1")
        .then(data => {
            return data[0].media[0].gif.url
        })
        .catch(err => {
            console.log(err)
        })
}

function abrazo(){
    return Tenor.Search.Random("anime hug","1")
        .then(data => {
            return data[0].media[0].gif.url
        })
        .catch(err => {
            console.log(err)
        })
}

function caricia(){
    return Tenor.Search.Random("anime pat","1")
        .then(data => {
            return data[0].media[0].gif.url
        })
        .catch(err => {
            console.log(err)
        })
}

function beso(){
    return Tenor.Search.Random("anime kiss","1")
        .then(data => {
            return data[0].media[0].gif.url
        })
        .catch(err => {
            console.log(err)
        })
}

module.exports = {
    loli:loli,
    husbando:husbando,
    abrazo:abrazo,
    caricia:caricia,
    beso:beso
}