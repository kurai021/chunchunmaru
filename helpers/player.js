const ytdl = require("ytdl-core");

const queue = new Map();

async function execute(args, message,voiceChannel) {
    const songInfo = await ytdl.getInfo(args);
    
    const song = {
        title: songInfo.videoDetails.title,
        url: songInfo.videoDetails.video_url,
    };

    const serverQueue = queue.get(message.guild.id);

    if (!serverQueue) {
        const queueContruct = {
            textChannel: message.channel,
            voiceChannel: voiceChannel,
            connection: null,
            songs: [],
            volume: 5,
            playing: true
        };

        queue.set(message.guild.id, queueContruct);

        queueContruct.songs.push(song);

        try {
            var connection = await voiceChannel.join();
            queueContruct.connection = connection;
            play(message.guild, queueContruct.songs[0]);
        } catch (err) {
            console.log(err);
            queue.delete(message.guild.id);
            return message.channel.send(err);
        }
    } else {
        serverQueue.songs.push(song);
        return message.channel.send(`${song.title} ha sido agregado al listado!`);
    }
}

function pause(message) {
    const serverQueue = queue.get(message.guild.id);

    if (!serverQueue)
      return message.channel.send("¡No hay más canciones para pausar!");

    serverQueue.connection.dispatcher.pause()
}

function resume(message) {
    const serverQueue = queue.get(message.guild.id);

    if (!serverQueue)
      return message.channel.send("¡No hay más canciones para pausar!");

    serverQueue.connection.dispatcher.resume()
}

function stop(message) {
    const serverQueue = queue.get(message.guild.id);
      
    if (!serverQueue)
      return message.channel.send("¡No hay canciones en lista para detener!");
      
    serverQueue.songs = [];
    serverQueue.connection.dispatcher.end();
}

function play(guild, song) {
    const serverQueue = queue.get(guild.id);
    if (!song) {
      serverQueue.voiceChannel.leave();
      queue.delete(guild.id);
      return;
    }
  
    const dispatcher = serverQueue.connection
      .play(ytdl(song.url))
      .on("finish", () => {
        serverQueue.songs.shift();
        play(guild, serverQueue.songs[0]);
      })
      .on("error", error => console.error(error));
    dispatcher.setVolumeLogarithmic(serverQueue.volume / 5);
    serverQueue.textChannel.send(`Está sonando: **${song.title}**`);
  }

module.exports = {
    execute:execute,
    pause:pause,
    resume:resume,
    stop: stop
}