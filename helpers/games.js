function eightBall(){
    const eightBallResponses =  ["En mi opinión, sí","Es cierto","Es decididamente así","Probablemente","Buen pronóstico","Todo apunta a que sí","Sin duda","Sí",
    "Sí, definitivamente","Debes confiar en ello","Respuesta vaga, vuelve a intentarlo","Pregunta en otro momento","Será mejor que no te lo diga ahora","No puedo predecirlo ahora",
    "Concéntrate y vuelve a preguntar","Puede ser","No cuentes con ello","Mi respuesta es no","Mis fuentes me dicen que no","Las perspectivas no son buenas","Muy dudoso"];
    const randomNumber = Math.random();
    const randomAnswer = Math.floor(randomNumber * 21);
    const answer = eightBallResponses[randomAnswer];

    return answer;

}

function flipCoin(){
    const coinSides = ["cara","cruz"];
    const randomNumber = Math.random();
    const randomSide = Math.floor(randomNumber * coinSides.length);
    const answer = coinSides[randomSide];

    return answer
}

function rsp(data){
    const rspResponses = ["piedra","papel","tijeras"];
    
    if(rspResponses.includes(data)){
        const randomNumber = Math.random();
        const randomRSP = Math.floor(randomNumber * 3);
        const answer = rspResponses[randomRSP];

        if(data == answer){
            return `Tu elegiste: ${data} y yo elegí: ${answer} **¡Empate!**`
        }

        else if(data == 'papel' && answer == 'tijera'){
            return `Tu elegiste: ${data} y yo elegí: ${answer} **¡Perdiste!**`
        }

        else if(data == 'tijera' && answer == 'piedra'){
            return `Tu elegiste: ${data} y yo elegí: ${answer} **¡Perdiste!**`
        }

        else if(data == 'piedra' && answer == 'papel'){
            return `Tu elegiste: ${data} y yo elegí: ${answer} **¡Perdiste!**`
        }

        else if(data == 'tijera' && answer == 'papel'){
            return `Tu elegiste: ${data} y yo elegí: ${answer} **¡Ganaste!**`
        }

        else if(data == 'papel' && answer == 'piedra'){
            return `Tu elegiste: ${data} y yo elegí: ${answer} **¡Ganaste!**`
        }

        else {
            return `Tu elegiste: ${data} y yo elegí: ${answer} **¡Ganaste!**`
        }

    }

    else {
        return "Las opciones son piedra, papel y tijeras"
    }
}

module.exports = {
    eightBall:eightBall,
    flipCoin:flipCoin,
    rsp:rsp
}