const Pokedex = require('pokedex-promise-v2')
const util = require("../util/utils")
const P = new Pokedex()

function getInfo(args,message){
    let types = []
    let normalHab = []
    let hiddenHab = []
    let description = [];
    let pokemon;

    switch(args){
        case 'deoxys':
            pokemon = 'deoxys-normal';
            break;
        case 'wormadam':
            pokemon = 'wormadam-plant' || 'wormadam-trash' || 'wormadam-sandy';
            break;
        case 'giratina':
            pokemon = 'giratina-altered';
            break;
        case 'shaymin':
            pokemon = 'shaymin-land';
            break;
        case 'basculin':
            pokemon = 'basculin-red-striped' || 'basculin-blue-striped';
            break;
        case 'darmanitan':
            pokemon = 'darmanitan-standard';
            break;
        case 'tornadus':
            pokemon = 'tornadus-incarnate';
            break;
        case 'landorus':
            pokemon = 'landorus-incarnate';
            break;
        case 'thundurus':
            pokemon = 'thundurus-incarnate';
            break;
        case 'keldeo':
            pokemon = 'keldeo-ordinary';
            break;
        case 'meloetta':
            pokemon = 'meloetta-aria';
            break;
        case 'aegislash':
            pokemon = 'aegislash-shield';
            break;
        case 'pumpkaboo':
            pokemon = 'pumpkaboo-average';
            break;
        case 'gourgeist':
            pokemon = 'gourgeist-average';
            break;
        case 'oricorio':
            pokemon = 'oricorio-baile' || 'oricorio-pom-pom' || 'oricorio-pau' || 'oricorio-sensu';
            break;
        case 'lycanroc':
            pokemon = 'lycanroc-midday' || 'lycanroc-midnight' || 'lycanroc-dusk';
            break;
        case 'wishiwashi':
            pokemon = 'wishiwashi-solo';
            break;
        case 'minior':
            pokemon = 'minior-red-meteor' || 'minior-orange-meteor' || 'minior-yellow-meteor' || 'minior-green-meteor' || 'minior-blue-meteor' || 'minior-indigo-meteor' || 'minior-violet-meteor';
            break;
        case 'mimikyu':
            pokemon = 'mimikyu-disguised';
            break;
        default:
            pokemon = args
            break;
    }

    P.getPokemonByName(pokemon)
        .then(async function(res){

            res.types.forEach(item => {
                types.push(item.type.name)
            })

            res.abilities.forEach(item => {
                if(item.is_hidden == false){
                    normalHab.push(item.ability.name)
                }
                else {
                    hiddenHab.push(item.ability.name)
                }
            })

            P.getPokemonSpeciesByName(pokemon)
                .then(async function(res){
                    res.flavor_text_entries.forEach(function(item){
                        if(item.language.name == "es"){
                            description.push(item.flavor_text);
                        }
                    })

                    

                    if(hiddenHab.length == 0){
                        hiddenHab.push("No tiene habilidades ocultas")
                    }

                    await message.channel.send({embed: {
                        color: 'C60000',
                        title: `Nombre: ${util.capitalize(args)}`,
                        description:`**Descripción:** ${description[0]}`,
                        fields: [
                            {
                                name: "Tipo",
                                value: `${types.join(" / ")}`
                            },
                            {
                                name: "Habilidades Normales",
                                value: `${normalHab}`
                            },
                            {
                                name: "Habilidades Ocultas",
                                value: `${hiddenHab}`
                            }
                        ],
                        thumbnail: {
                            url: `https://play.pokemonshowdown.com/sprites/ani/${pokemon}.gif`
                        }}
                    })

                })
                .catch(function(err){
                    message.reply("Ha habido un error al momento de hacer la búsqueda")
                    console.log("Ha habido un error:", err)
                })

        })
        .catch(function(err){
            message.reply("No se pudo hacer la busqueda o quizás el nombre es incorrecto")
            console.log(err)
        })
}

module.exports = {
    getInfo:getInfo
}