function sinceToday(data){
    let days = Math.ceil(data / 86400)

    if(days == 1){
        return `sale en pocas horas`
    }
    else {
        return `sale en ${days} días`
    }
}

function capitalize(s) {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

module.exports = {
    sinceToday:sinceToday,
    capitalize:capitalize
}